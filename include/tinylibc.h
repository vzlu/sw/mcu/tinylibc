/*
 * tinylibc - Tiny libc implementation for embedded systems.
 */

#ifndef _TINYLIBC_H
#define _TINYLIBC_H

#include "conf_tl.h"

// memory functions - memset, memcpy etc.
//#define TINY_MEM

// string functions - strlen, strcpy etc.
//#define TINY_STR

// printf support
//#define TINY_PRINT

#ifdef TINY_PRINT
  // printf double support - formatting double takes a lot of resources
  //#define TINY_PRINT_DOUBLE
#endif

// math functions (or math.h overrides)
//#define TINY_MATH

#ifdef TINY_MATH
  // override standard math.h functions
  //#define TINY_MATH_OVERRIDE
#endif

// time-related functions - tick counter, delay etc.
//#define TINY_TICK

// type to be used for size-related variables - string or array lengths etc.
// use unsigned int on platforms which do not define size_t
#define TINY_SIZE_TYPE                    unsigned int

// type to be used for memory-address-related variables
// pointer to a single byte (not word!)
#define TINY_ADDR_TYPE                    unsigned char

// type to be used for memory-mapped IO access
// pointer to a word (not byte!)
#define TINY_WORD_TYPE                    unsigned int

// address value to memory-mapped IO
#define TINY_REGW(x)                      (*(volatile TINY_WORD_TYPE*)(x))
#define TINY_REGB(x)                      (*(volatile uint8_t*)(x))

// linker symbol to address value
#define TINY_ADRW(x)                      ((TINY_WORD_TYPE*)&x)
#define TINY_ADRB(x)                      ((uint8_t*)&x)

#ifdef TINY_MEM

// check NULL exists
#ifndef NULL
#define NULL                              ((void *) 0)
#endif

void* tl_memcpy(void* dst, const void* src, TINY_SIZE_TYPE len);
void* tl_memset(void* dst, char c, TINY_SIZE_TYPE len);
int tl_memcmp(const void* str1, const void* str2, TINY_SIZE_TYPE len);
#endif  // TINY_MEM

#ifdef TINY_STR
#include <stdarg.h>
TINY_SIZE_TYPE tl_strlen(const char* str);
char* tl_strcpy(char* dst, const char* src);
char* tl_strncpy(char* dst, const char* src, TINY_SIZE_TYPE len);
TINY_SIZE_TYPE tl_strcmp(const char* s1, const char* s2);
TINY_SIZE_TYPE tl_strncmp(const char* s1, const char* s2, TINY_SIZE_TYPE size);
long tl_strtol(const char* str, int base);
TINY_SIZE_TYPE tl_sprintf(char *pStr, const char *pFormat, ...);
TINY_SIZE_TYPE tl_vsnprintf(char *pStr, TINY_SIZE_TYPE length, const char *pFormat, va_list ap);
#endif  // TINY_STR

#ifdef TINY_PRINT
typedef void (*putchar_cb_t)(char);
void tl_initprintf(putchar_cb_t cb);
void tl_putstring(char *str);
TINY_SIZE_TYPE tl_printf(const char *pFormat, ...);
#endif  // TINY_PRINT

#ifdef TINY_MATH
float tl_roundf(float num);
float tl_fabs(float num);

#ifdef TINY_MATH_OVERRIDE
inline float roundf(float num) { return(tl_roundf(num)); }
#endif

#endif  // TINY_MATH

#ifdef TINY_TICK
void tl_tick();
unsigned long tl_ticks();
void tl_delay(unsigned long d);
void tl_delay_ms(unsigned long ms);
void tl_set_tickrate(unsigned long rate);
unsigned long tl_get_tickrate();
#endif  // TINY_TICK

#endif  // _TINYLIBC_H
