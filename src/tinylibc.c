#include "tinylibc.h"

#define TINY_MAX_STR_LEN     (256)

#ifdef TINY_PRINT
#include <stdarg.h>
#endif  // TINY_PRINT

#ifdef TINY_MEM

void* tl_memcpy(void* dst, const void* src, TINY_SIZE_TYPE len) {
  TINY_ADDR_TYPE *dst_addr = (TINY_ADDR_TYPE*)dst;
  TINY_ADDR_TYPE *src_addr = (TINY_ADDR_TYPE*)src;
  TINY_SIZE_TYPE i;
  for(i = 0; i < len; i++) {
    *dst_addr++ = *src_addr++;
  }
  return(dst);
}

void* tl_memset(void* dst, char c, TINY_SIZE_TYPE len) {
  TINY_ADDR_TYPE *dst_addr = (TINY_ADDR_TYPE*)dst;
  TINY_SIZE_TYPE i;
  for(i = 0; i < len; i++) {
    *dst_addr = c;
    dst_addr++;
  }
  return(dst);
}

int tl_memcmp(const void* str1, const void* str2, TINY_SIZE_TYPE len) {
  TINY_ADDR_TYPE *s1 = (TINY_ADDR_TYPE*)str1;
  TINY_ADDR_TYPE *s2 = (TINY_ADDR_TYPE*)str2;

  while(len-- > 0) {
    if (*s1++ != *s2++) {
      return s1[-1] < s2[-1] ? -1 : 1;
    }
  }
  return(0);
}

#endif  // TINY_MEM

#ifdef TINY_STR

TINY_SIZE_TYPE tl_strlen(const char* str) {
  TINY_SIZE_TYPE pos = 0;
  while(str[pos]) {
    pos++;
  }

  return(pos);
}

char* tl_strcpy(char* dst, const char* src) {
  TINY_SIZE_TYPE i = 0;
  char* ptr = dst;
  while(src[i]) {
    *ptr = *(src + i);
    ptr++;
    i++;
  }
  return(dst);
}

char* tl_strncpy(char* dst, const char* src, TINY_SIZE_TYPE len) {
  TINY_SIZE_TYPE i = 0;
  char* ptr = dst;
  while(src[i] && i < len) {
    *ptr = *(src + i);
    ptr++;
    i++;
  }
  if(i < len) {
    *ptr = '\0';
  }
  return(dst);
}

TINY_SIZE_TYPE tl_strcmp(const char* s1, const char* s2) {
  return(tl_strncmp(s1, s2, tl_strlen(s2)));
}

TINY_SIZE_TYPE tl_strncmp(const char* s1, const char* s2, TINY_SIZE_TYPE size) {
  while(*s1 && *s2 && size) {
    if(*s1 > *s2) {
      return(1);
    }

    if(*s1 < *s2) {
      return(-1);
    }

    s1++;
    s2++;
    size--;
  }

  if(size < 1) {
    return(0);
  }

  if(*s1) {
    return(1);
  }

  if(*s2) {
    return(-1);
  }

  return(0);
}

long tl_strtol(const char* str, int base) {
  TINY_SIZE_TYPE i = 0;
  long val = 0;
  switch(base) {
    case 16: {
      while(str[i] && i < TINY_MAX_STR_LEN) {
        char ch = str[i++];
        if(ch <= '9' && ch >= '0') {
          val *= 16;
          val += ch - '0';
        } else if(ch <= 'F' && ch >= 'A') {
          val *= 16;
          val += ch - 'A' + 10;
        } else if(ch <= 'f' && ch >= 'a') {
          val *= 16;
          val += ch - 'a' + 10;
        } else if(val) {
          break;
        }

      }
    } break;
  }

  return(val);
}

signed int putChar(char *pStr, char c){
  *pStr = c;
  return(1);
}

signed int putSignedInt(char *pStr, char fill, signed int width, signed int value) {
  signed int num = 0;
  unsigned int absolute;

  // compute absolute value
  if(value < 0) {
    absolute = -value;
  } else {
    absolute = value;
  }

  // take current digit into account when calculating width
  width--;

  // recursively write upper digits
  // TODO get rid of this recursion
  if((absolute / 10) > 0) {
    if(value < 0) {
      num = putSignedInt(pStr, fill, width, -(absolute / 10));
    } else {
      num = putSignedInt(pStr, fill, width, absolute / 10);
    }
    pStr += num;

  } else {

    // reserve space for sign
    if(value < 0) {
      width--;
    }

    // write filler characters
    while (width > 0) {
      putChar(pStr, fill);
      pStr++;
      num++;
      width--;
    }

    // write sign
    if (value < 0) {
      num += putChar(pStr, '-');
      pStr++;
    }
  }

  // write lower digit
  num += putChar(pStr, (absolute % 10) + '0');

  return(num);
}

signed int putDouble(char *pStr, signed int width, double value) {
  signed int num = 0;
  int i, e = 0;
  if(width < 1) {
    width = 11;
  }

  // check sign
  if(value < 0.0) {
    value = -value;
    num += putChar(pStr, '-');
    pStr++;
  }

  // calculate exponent
  while((value < 1.0) && (e > -310)) {
    value *= 10;
    e--;
  }
  while((value > 10.0) && (e < 310)) {
    value /= 10;
    e++;
  }

  // check +/- infinity
  if(e >= 310 || e <= -310) {
    num += putChar(pStr, 'i');
    pStr++;
    num += putChar(pStr, 'n');
    pStr++;
    num += putChar(pStr, 'f');
    pStr++;
    return(num);
  }

  // check NaN
  if((int)value > 9) {
    num += putChar(pStr, 'n');
    pStr++;
    num += putChar(pStr, 'a');
    pStr++;
    num += putChar(pStr, 'n');
    pStr++;
    return(num);
  }

  // should be a well-behaved number at this point
  num += putChar(pStr, (int)value + '0');
  pStr++;
  num += putChar(pStr, '.');
  pStr++;
  for(i = 0; i < width; i++) {
    value -= (int)value;
    value *= 10;
    num += putChar(pStr, (int)value + '0');
    pStr++;
  }
  num += putChar(pStr, 'e');
  pStr++;
  num += putSignedInt(pStr, ' ', 0, e);
  return(num);
}

signed int putUnsignedInt(char *pStr, char fill, signed int width, unsigned int value) {
  signed int num = 0;

  // take current digit into account when calculating width
  width--;

  // recursively write upper digits
  // TODO get rid of this recursion
  if((value / 10) > 0) {
    num = putUnsignedInt(pStr, fill, width, value / 10);
    pStr += num;

  } else {
    // write filler characters
    while (width > 0) {
      putChar(pStr, fill);
      pStr++;
      num++;
      width--;
    }

  }

  // write lower digit
  num += putChar(pStr, (value % 10) + '0');

  return(num);
}

signed int putUnsignedLong(char *pStr, char fill, signed int width, unsigned long value) {
  signed int num = 0;

  // take current digit into account when calculating width
  width--;

  // recursively write upper digits
  // TODO get rid of this recursion
  if((value / 10) > 0) {
    num = putUnsignedLong(pStr, fill, width, value / 10);
    pStr += num;

  } else {
    // write filler characters
    while (width > 0) {
      putChar(pStr, fill);
      pStr++;
      num++;
      width--;
    }

  }

  // write lower digit
  num += putChar(pStr, (value % 10) + '0');

  return(num);
}

signed int putHex(char *pStr, char fill, signed int width, unsigned char cap, unsigned int value) {
  signed int num = 0;

  // decrement width
  width--;

  // recursively output upper digits
  // TODO get rid of this recursion
  if((value >> 4) > 0) {
    num += putHex(pStr, fill, width, cap, value >> 4);
    pStr += num;

  } else {
    // write filler chars
    while (width > 0) {
      putChar(pStr, fill);
      pStr++;
      num++;
      width--;
    }

  }

  // write current digit
  if((value & 0xF) < 10) {
    putChar(pStr, (value & 0xF) + '0');
  } else if(cap) {
    putChar(pStr, (value & 0xF) - 10 + 'A');
  } else {
    putChar(pStr, (value & 0xF) - 10 + 'a');
  }
  num++;

  return(num);
}

signed int putStr(char *pStr, char fill, signed int width, const char *pSource) {
  signed int num = 0;

  while(*pSource != 0) {
    *pStr++ = *pSource++;
    num++;
  }

	width -= num;
	while(width > 0) {
	  *pStr++ = fill;
		num++;
		width--;
	}

  return(num);
}

TINY_SIZE_TYPE tl_vsnprintf(char *pStr, TINY_SIZE_TYPE length, const char *pFormat, va_list ap) {
  char fill;
  TINY_SIZE_TYPE width;
  TINY_SIZE_TYPE num = 0;
  TINY_SIZE_TYPE size = 0;

  // Clear the string
  if(pStr) {
    *pStr = 0;
  }

  // Phase string
  while((*pFormat != 0) && (size < length)) {

    if(*pFormat != '%') {
      // Normal character
      *pStr++ = *pFormat++;
      size++;

    } else if(*(pFormat + 1) == '%') {
      // Escaped '%'
      *pStr++ = '%';
      pFormat += 2;
      size++;

    } else {
      // Token delimiter
      fill = ' ';
      width = 0;
      pFormat++;

      // Parse filler
      if(*pFormat == '0') {
        fill = '0';
        pFormat++;
      }

      // Ignore justifier
      if(*pFormat == '-') {
        pFormat++;
      }

      // Parse width
      while((*pFormat >= '0') && (*pFormat <= '9')) {
        width = (width*10) + *pFormat-'0';
        pFormat++;
      }

      // Check if there is enough space
      if(size + width > length) {
        width = length - size;
      }

      // Parse type
      switch (*pFormat) {
        case 'd':
        case 'i': num = putSignedInt(pStr, fill, width, va_arg(ap, signed int)); break;
        #ifdef TINY_PRINT_DOUBLE
        case 'f': num = putDouble(pStr, width, va_arg(ap, double)); break;
        #endif
        case 'p': num = putHex(pStr, '0', 2*sizeof(int), 0, va_arg(ap, unsigned int)); break;
        case 'u': num = putUnsignedInt(pStr, fill, width, va_arg(ap, unsigned int)); break;
        case 'l': num = putUnsignedLong(pStr, fill, width, va_arg(ap, unsigned long)); break;
        case 'x': num = putHex(pStr, fill, width, 0, va_arg(ap, unsigned int)); break;
        case 'X': num = putHex(pStr, fill, width, 1, va_arg(ap, unsigned int)); break;
        case 's': num = putStr(pStr, fill, width, va_arg(ap, char *)); break;
        case 'c': num = putChar(pStr, va_arg(ap, unsigned int)); break;
        default:
          return -1;
      }

      pFormat++;
      pStr += num;
      size += num;
    }
  }

  // NULL-terminated (final \0 is not counted)
  if(size < length) {
    *pStr = 0;
  } else {
    *(--pStr) = 0;
    size--;
  }

  return(size);
}

TINY_SIZE_TYPE tl_sprintf(char *pStr, const char *pFormat, ...) {
  va_list ap;
  TINY_SIZE_TYPE result;

  // Forward call to vsprintf
  va_start(ap, pFormat);
  result = tl_vsnprintf(pStr, TINY_MAX_STR_LEN, pFormat, ap);
  va_end(ap);

  return(result);
}

#endif  // TINY_STR

#ifdef TINY_PRINT

static putchar_cb_t tl_putchar = NULL;

void tl_initprintf(putchar_cb_t cb) {
  tl_putchar = cb;
}

void tl_putstring(char *str) {
  while(*str != '\0') {
    tl_putchar(*str++);
  }
}

TINY_SIZE_TYPE tl_printf(const char *pFormat, ...) {
    if(!tl_putchar) {
      return(0);
    }
    char tmp[TINY_MAX_STR_LEN];
    va_list ap;
    TINY_SIZE_TYPE result;

    // Forward call to vsprintf
    va_start(ap, pFormat);
    result = tl_vsnprintf(tmp, TINY_MAX_STR_LEN, pFormat, ap);
    va_end(ap);

    tl_putstring(tmp);
    return result;
}

#endif  // TINY_PRINT

#ifdef TINY_MATH

float tl_roundf(float num) {
  if(num > 0) {
    return(num + 0.5f);
  }
  return(num - 0.5f);
}

float tl_fabs(float num) {
  if(num > 0) {
    return(num);
  }
  return(-num);
}

#endif  // TINY_MATH

#ifdef TINY_TICK

volatile unsigned long tl_tickCounter = 0;
static unsigned long tl_tickrate = 0;

void tl_tick() {
  tl_tickCounter++;
}

unsigned long tl_ticks() {
  return(tl_tickCounter);
}

void tl_delay(unsigned long d) {
  unsigned long tim = tl_ticks();
  unsigned long waitTo = tim;
  waitTo += d;
  if(waitTo > tim) {
    while(tl_ticks() < waitTo);

  } else if (waitTo < tim) {
     //counter overflow
    waitTo += d;
    while((tl_ticks() + d) < waitTo);
  }
}

void tl_delay_ms(unsigned long ms) {
  tl_delay((ms * tl_tickrate) / 1000);
}

void tl_set_tickrate(unsigned long rate) {
  tl_tickrate = rate;
}

unsigned long tl_get_tickrate() {
  return(tl_tickrate);
}

#endif // TINY_TICK
